# A C++ library for parsing and generating COBS messages

## COBS (Consistent Overhead Byte Stuffing)
Description: https://en.wikipedia.org/wiki/Consistent_Overhead_Byte_Stuffing

### Features

* Implements a flavor of the COBS protocol
* Uses Zero Pair Encoding (ZPE)
* Uses Zero Run Encoding (ZRE)
* Does not do packet preemption

### Encoding used

| Code (n)     | Value | Comment                                                    |
| :----------- | :---: | :--------------------------------------------------------- |
| Unused       | 0x00  | Unused (framing character placeholder)                     |
| DiffZero     | 0x01  | Range 0x01 - 0xD1                                          |
| DiffZeroMax  | 0xD1  | n-1 explicit characters plus a zero                        |
| Diff         | 0xD2  | 209 explicit characters, no added zero                     |
| RunZero      | 0xD3  | Range 0xD3 - 0xDF                                          |
| RunZeroMax   | 0xDF  | 3-15 zeros (Zero Run Encoding)                             |
| Diff2Zero    | 0xE0  | Range 0xE0 - 0xFF                                          |
| Diff2ZeroMax | 0xFF  | 0-31 explicit characters plus 2 zeros (Zero Pair Encoding) |

## Contributors

* Emil Fresk

---

## License

Licensed under the MIT license, see LICENSE file for details.

---

* Simple to use, see example file.
* Compile example: `g++ -std=c++11 -ggdb example.cpp -o example -I../include`
