/****************************************************************************
*
* cobs_impl.h -- Main implementation file of the COBS C++ library.
*
* Copyright (C) 2016 Emil Fresk
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*
****************************************************************************/

#ifndef _COBS_IMPL_H
#define _COBS_IMPL_H

#include "libcobs/cobs.h"

namespace COBS
{
/*****************************************************************************/
/* Private methods.                                                          */
/*****************************************************************************/

inline bool cobsCodec::isDiff2Zero(const uint8_t code)
{
  return (code >= StuffingCode::Diff2Zero);
}

inline bool cobsCodec::isRunZero(const uint8_t code)
{
  return ((code >= StuffingCode::RunZero) &&
          (code <= StuffingCode::RunZeroMax));
}

inline void cobsCodec::finishBlock(void)
{
  /* Save the code and set the index for the next code byte. */
  encode_buffer_[codeIndex_] = code_;
  codeIndex_                 = encode_buffer_.size();

  /* Add dummy byte for the next code, or the frame delimiter if there are
   * no more data to be encoded. */
  encode_buffer_.push_back(StuffingCode::FrameDelimiter);
  code_ = StuffingCode::DiffZero;
}

inline void cobsCodec::executeCallbacks(const ByteVector &payload)
{
  /* Execute callbacks. */
  std::lock_guard<std::mutex> locker(id_cblock_);

  /* Run each callback in its own thread. */
  for (auto &cb : callbacks)
  {
    /* TODO: See if the callbacks can be run in their own threads without
     *       adding significant latency. This would allow the callbacks to do
     *       processing in them without causing problems for the codec.
     */

    cb.second(payload);

    //std::thread work(cb.second, payload);
    //work.detach();
  }
}

/*****************************************************************************/
/* Public methods.                                                           */
/*****************************************************************************/

cobsCodec::cobsCodec()
{
  /* Initialize the ID counter. */
  id_ = 0;

  /* Reset the system. */
  reset();
}

void cobsCodec::reset(void)
{
  /* Clear the buffers. */
  encode_buffer_.clear();
  decode_buffer_.clear();

  /* Reserve space for the scratch pads. */
  encode_buffer_.reserve(1024);
  decode_buffer_.reserve(1024);

  code_ = StuffingCode::DiffZero;

  /* Put in a dummy byte for the first code. */
  encode_buffer_.push_back(StuffingCode::FrameDelimiter);
  codeIndex_ = 0;

  /* Reset the decoding state machine. */
  decode_state_ = AwaitingCode;

  /* Reset the error counter. */
  decode_num_errors_ = 0;
}

unsigned int cobsCodec::registerCallback(ByteVectorCallback callback)
{
  std::lock_guard<std::mutex> locker(id_cblock_);

  /* Add the callback to the list. */
  callbacks.emplace(id_, callback);

  return id_++;
}

bool cobsCodec::unregisterCallback(const unsigned int id)
{
  std::lock_guard<std::mutex> locker(id_cblock_);

  /* Delete the callback with correct ID, a little ugly. */
  if (callbacks.erase(id) > 0)
    return true;
  else
    /* No match, return false. */
    return false;
}

void cobsCodec::encode(const ByteVector &in)
{
  for (auto &byte : in)
    encode(byte);
}

void cobsCodec::encode(const uint8_t in)
{
  if (in == 0)
  {
    /* If the data byte is zero apply the COBS encoding. */

    if (isRunZero(code_) && code_ < StuffingCode::RunZeroMax)
    {
      /* If already in ZRE mode, continue filling. */
      code_++;
    }
    else if (code_ == StuffingCode::Diff2Zero)
    {
      /* 2 zeros and no data, switch to ZRE. */
      code_ = StuffingCode::RunZero;
    }
    else if (code_ <= maxConvertible)
    {
      /* In 1 zero and data is small enough to switch to ZPE. */
      code_ += convertZP;
    }
    else
    {
      /* Worst case scenario: Too much data for ZPE or too many zeros for
       * ZRE. Finish the block and start over. */
      finishBlock();
      code_ = StuffingCode::DiffZero;
    }
  }
  else
  {
    /* If the data byte is non-zero, check the code and add the byte. */

    /* Note that the code is one step ahead here and needs to be converted
     * back one step. */
    if (isDiff2Zero(code_))
    {
      code_ -= convertZP;
      finishBlock();
    }
    else if (code_ == StuffingCode::RunZero)
    {
      code_ = StuffingCode::Diff2Zero;
      finishBlock();
    }
    else if (isRunZero(code_))
    {
      code_ -= 1;
      finishBlock();
    }

    /* Put the data in the buffer. */
    encode_buffer_.push_back(in);

    if (++code_ == StuffingCode::Diff)
    {
      /* We will hit the max limit of codes, finish and start a new. */
      finishBlock();
    }
  }
}

void cobsCodec::decode(const ByteVector &in)
{
  for (auto ch : in)
    decode(ch);
}

void cobsCodec::decode(const uint8_t in)
{
  if ((decode_state_ == AwaitingCode) && (in != 0))
  {
    /* Change state. */
    decode_state_ = AwaitingData;

    if (in == StuffingCode::Diff)
    {
      /* No zeros, all data. */
      decode_num_zeros_ = 0;
      decode_num_data_  = in - 1;
    }
    else if (isRunZero(in))
    {
      /* Extract the number of zeros from ZRE (and no data). */
      decode_num_zeros_ = in & 0x0f;
      decode_num_data_  = 0;
    }
    else if (isDiff2Zero(in))
    {
      /* Extract the number of data bytes and add 2 zeros (ZPE). */
      decode_num_zeros_ = 2;
      decode_num_data_  = in & 0x1f;
    }
    else
    {
      /* Extract the number of data bytes and add 1 zero (DiffZero). */
      decode_num_zeros_ = 1;
      decode_num_data_  = in - 1;
    }

    /* Check if there is no data, only zeros. */
    if (decode_num_data_ == 0)
    {
      /* Add all zeros. */
      while (decode_num_zeros_-- > 0)
        decode_buffer_.push_back(0);

      decode_state_ = AwaitingCode;
    }
  }
  else if ((decode_state_ == AwaitingData) && (in != 0))
  {
    /* Decrease the data counter. */
    decode_num_data_--;

    /* While there are data bytes left, add it to the output. */
    decode_buffer_.push_back(in);

    if (decode_num_data_ == 0)
    {
      /* Add all zeros. */
      while (decode_num_zeros_-- > 0)
        decode_buffer_.push_back(0);

      decode_state_ = AwaitingCode;
    }
  }
  else
  {
    /* Packet separator detected! */

    if (decode_state_ == AwaitingCode)
    {
      /* If there are data bytes in the buffer, remove the extra zero. */
      if (!decode_buffer_.empty())
        decode_buffer_.pop_back();

      executeCallbacks(decode_buffer_);
    }
    else
    {
      /* Packet separator detected in data section: Error!
       * Increase error counter. */
      decode_num_errors_++;
    }

    /* Reset state and buffer. */
    decode_state_ = AwaitingCode;
    decode_buffer_.clear();
  }
}

ByteVector cobsCodec::getEncodedPacket(void)
{
  /* Finish the current block. */
  finishBlock();

  auto ret = encode_buffer_;

  /* Reset the encoding buffer. */
  encode_buffer_.clear();
  encode_buffer_.push_back(0);
  codeIndex_ = 0;

  return ret;
}

unsigned int cobsCodec::getDecodeErrors(void)
{
  return decode_num_errors_;
}
}  // end namespace COBS

#endif
