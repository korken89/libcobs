/****************************************************************************
*
* cobs.h -- Main header file of the COBS C++ library.
*
* Copyright (C) 2016 Emil Fresk
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*
****************************************************************************/

#ifndef _COBS_H
#define _COBS_H

#include <cstddef>
#include <cstdint>
#include <functional>
#include <map>
//#include <thread>
#include <mutex>
#include <vector>

namespace COBS
{
/** @brief Definition of a byte vector. */
typedef std::vector<uint8_t> ByteVector;

/** @brief Definition of the COBS callback. */
typedef std::function<void(const ByteVector)> ByteVectorCallback;

/** @brief The possible states of the parser. */
enum DecodeStates
{
  AwaitingCode,
  AwaitingData
};

/** @brief The different stuffing bytes of the COBS protocol. */
namespace StuffingCode
{
enum
{
  FrameDelimiter = 0x00, /* Unused (framing character placeholder)        */
  DiffZero       = 0x01, /* Range 0x01 - 0xD1:                            */
  DiffZeroMax    = 0xD1, /* n-1 explicit characters plus a zero           */
  Diff           = 0xD2, /* 209 explicit characters, no added zero        */
  RunZero        = 0xD3, /* Range 0xD3 - 0xDF:                            */
  RunZeroMax     = 0xDF, /* 3-15 zeros (Zero Run Encoding)                */
  Diff2Zero      = 0xE0, /* Range 0xE0 - 0xFF:                            */
  Diff2ZeroMax   = 0xFF, /* 0-31 characters plus 2 implicit zeros (Zero
                            Pair Encoding)                                */
};
}

/** @brief Convert from single-zero code to corresponding double-zero code. */
constexpr int convertZP = (StuffingCode::Diff2Zero - StuffingCode::DiffZero);

/** @brief Highest single-zero code with a corresponding double-zero code. */
constexpr int maxConvertible = (StuffingCode::Diff2ZeroMax - convertZP);

class cobsCodec
{
private:
  /** @brief Mutex for the ID counter and the callback list. */
  std::mutex id_cblock_;

  /** @brief Vector holding the registered callbacks. */
  std::map<unsigned int, ByteVectorCallback> callbacks;

  /** @brief ID counter for the removal of subscriptions. */
  unsigned int id_;

  /** @brief Counter for the number of recoding errors. */
  unsigned int decode_num_errors_;

  /** @brief Holder for the encoded payload during encoding. */
  ByteVector encode_buffer_;

  /** @brief Holder for the decoded payload during decoding. */
  ByteVector decode_buffer_;

  /** @brief Holder for the decoded payload during decoding. */
  DecodeStates decode_state_;

  /** @brief The number of data bytes to add to the output. */
  int decode_num_data_;

  /** @brief The number of zeros to add to the output. */
  int decode_num_zeros_;

  /** @brief The current COBS code byte. */
  uint8_t code_;

  /** @brief Index of the current code byte in the payload_ vector. */
  size_t codeIndex_;

  /**
   * @brief   Checks if the code is ZPE.
   *
   * @param[in] code  The code to be checked.
   *
   * @return  Return true if the code is ZPE.
   */
  bool isDiff2Zero(const uint8_t code);

  /**
   * @brief   Checks if the code is ZRE.
   *
   * @param[in] code  The code to be checked.
   *
   * @return  Return true if the code is ZRE.
   */
  bool isRunZero(const uint8_t code);

  /**
   * @brief   Saves current code byte and starts a new block.
   */
  void finishBlock(void);

  /**
   * @brief   Calls all the registered callbacks with the data payload.
   *
   * @param[in] payload   The payload to be sent to the next stage of
   *                      processing.
   */
  void executeCallbacks(const ByteVector &payload);

public:
  cobsCodec();

  /**
   * @brief   Resets the encoding and decoding buffers, codes, states,
   *          counting indexes and error counters.
   */
  void reset();

  /**
   * @brief   Register a callback for packet received.
   *
   * @param[in] callback  The function to register.
   * @note    Shall be of the form void(const ByteVector).
   *
   * @return  Return the ID of the callback, is used for unregistration.
   */
  unsigned int registerCallback(ByteVectorCallback callback);

  /**
   * @brief   Unregister a callback from the queue.
   *
   * @param[in] id    The ID supplied from @p registerCallback.
   *
   * @return  Return true if the ID was deleted.
   */
  bool unregisterCallback(const unsigned int id);

  /**
   * @brief   Encodes a vector of bytes.
   *
   * @param[in] in    The vector of bytes.
   */
  void encode(const ByteVector &in);

  /**
   * @brief   Encodes a byte.
   *
   * @param[in] in    The byte to be encoded.
   */
  void encode(const uint8_t in);

  /**
   * @brief   Decodes a vector of bytes.
   *
   * @param[in] in    The vector of bytes.
   */
  void decode(const ByteVector &in);

  /**
   * @brief   Decodes a byte.
   *
   * @param[in] in    The byte to be decoded.
   */
  void decode(const uint8_t in);

  /**
   * @brief   Gets the current encoded packet from the encoder.
   *
   * @return  The encoded packet.
   */
  ByteVector getEncodedPacket(void);

  /**
   * @brief   Gets the current number of decode errors.
   *
   * @return  The number of decode errors.
   */
  unsigned int getDecodeErrors(void);
};

}  // end namespace COBS

#endif

#include "libcobs/implementation/cobs_impl.h"
