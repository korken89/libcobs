/****************************************************************************
*
* example.cpp -- Example implementation file of the COBS C++ library.
*
* Copyright (C) 2016 Emil Fresk
*
* This software may be modified and distributed under the terms
* of the MIT license.  See the LICENSE file for details.
*
****************************************************************************/

#include <iomanip>
#include <iostream>

#include "libcobs/cobs.h"

using namespace std;
using namespace COBS;

void test(const std::vector<uint8_t> &data);

void printEncoding(const vector<uint8_t> &msg, const vector<uint8_t> &enc)
{
  cout << showbase  // show the 0x prefix
       << internal  // fill between the prefix and the number
       << setfill('0');

  cout << "Message (size " << msg.size() << "):" << endl << "    ";
  for (auto &ch : msg)
    cout << std::hex << int(ch) << std::dec << " ";

  cout << endl << "Encoded Message (size " << enc.size() << "):";
  cout << endl << "    ";
  for (auto &ch : enc)
    cout << std::hex << int(ch) << std::dec << " ";

  cout << endl;
}

int main()
{
  /* COBS */
  cobsCodec cobs;

  cobs.registerCallback(test);

  /* A test message to encode. */
  vector<uint8_t> msg1{1, 2, 3, 4, 5, 6};
  cobs.encode(msg1);
  vector<uint8_t> enc1 = cobs.getEncodedPacket();

  printEncoding(msg1, enc1);
  cobs.decode(enc1);

  vector<uint8_t> msg2{1, 2, 3, 0, 5, 6, 7, 8, 9};
  cobs.encode(msg2);
  vector<uint8_t> enc2 = cobs.getEncodedPacket();

  printEncoding(msg2, enc2);
  cobs.decode(enc2);

  vector<uint8_t> msg3{1, 2, 3, 0, 0, 6, 7, 8, 9};
  cobs.encode(msg3);
  vector<uint8_t> enc3 = cobs.getEncodedPacket();

  printEncoding(msg3, enc3);
  cobs.decode(enc3);

  vector<uint8_t> msg4{1, 2, 3, 0, 0, 0, 7, 8, 9};
  cobs.encode(msg4);
  vector<uint8_t> enc4 = cobs.getEncodedPacket();

  printEncoding(msg4, enc4);
  cobs.decode(enc4);

  vector<uint8_t> msg5{1, 2, 3, 0, 0, 0, 0, 0, 0, 7, 8, 9, 0};
  cobs.encode(msg5);
  vector<uint8_t> enc5 = cobs.getEncodedPacket();

  printEncoding(msg5, enc5);
  cobs.decode(enc5);

  vector<uint8_t> msg6{1, 2, 3, 0, 0, 0, 0, 0, 0, 7, 8, 9, 0, 0};
  cobs.encode(msg6);
  vector<uint8_t> enc6 = cobs.getEncodedPacket();

  printEncoding(msg6, enc6);
  cobs.decode(enc6);

  vector<uint8_t> msg7{1, 2, 3, 0, 0, 0, 0, 0, 0, 7, 8, 9, 0, 0, 0};
  cobs.encode(msg7);
  vector<uint8_t> enc7 = cobs.getEncodedPacket();

  printEncoding(msg7, enc7);
  cobs.decode(enc7);

  vector<uint8_t> msg8{1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 7, 8, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  cobs.encode(msg8);
  vector<uint8_t> enc8 = cobs.getEncodedPacket();

  printEncoding(msg8, enc8);
  cobs.decode(enc8);

  vector<uint8_t> msg9{1, 2, 3, 0, 0, 0, 0, 7, 8, 9, 0, 0, 0};
  cobs.encode(msg9);
  vector<uint8_t> enc9 = cobs.getEncodedPacket();

  printEncoding(msg9, enc9);
  cobs.decode(enc9);

  vector<uint8_t> msg10{1};
  cobs.encode(msg10);
  vector<uint8_t> enc10 = cobs.getEncodedPacket();

  printEncoding(msg10, enc10);
  cobs.decode(enc10);

  vector<uint8_t> msg11;

  for (int i = 0; i < 255; i++)
    msg11.push_back(1);

  cobs.encode(msg11);
  vector<uint8_t> enc11 = cobs.getEncodedPacket();

  printEncoding(msg11, enc11);
  cobs.decode(enc11);

  return 0;
}

/* Print incoming data... */
void test(const std::vector<uint8_t> &msg)
{
  cout << "Decode Message (size " << msg.size() << "):" << endl << "    ";
  for (auto &ch : msg)
    cout << std::hex << int(ch) << std::dec << " ";

  cout << endl << endl;
}
